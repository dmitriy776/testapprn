import firebase from 'firebase';
let config = {
    apiKey: "AIzaSyATSafSvPVqe_dQ0bfgD2SpKY8lSlV1XBo",
    authDomain: "testrnapp-67de2.firebaseapp.com",
    databaseURL: "https://testrnapp-67de2.firebaseio.com",
    projectId: "testrnapp-67de2",
    storageBucket: "testrnapp-67de2.appspot.com",
    messagingSenderId: "347252144678"
};
export default firebaseApp = firebase.initializeApp(config);
