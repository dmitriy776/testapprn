import React from 'react';
import {
    StyleSheet,
    View,
} from 'react-native';
import firebaseApp from '../../firebase/firebase';
import AuthForm from '../auth-form/auth-form';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import getLogin from '../../actions/getLoginAction';

class RegistrationScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            pass: ''
        }
    }

    _registration() {
        firebaseApp.auth()
        .createUserWithEmailAndPassword(this.state.email, this.state.pass)
        .then(() => {
            this.props.getLogin(this.state.email);
            this.props.navigation.navigate('Home');
        })
        .catch(error => alert(error.message));
    }

    _authentication(email, pass) {
        this.setState({
            email,
            pass
        }, this._registration);
    }

    static navigationOptions = {
        title: 'Registration',
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTintColor: '#FFFFFF',
    };

    render() {
        return (
            <View style={styles.container}>
                <AuthForm addData={this._authentication.bind(this)} btnName={'Registration'}/>
            </View>
        )
    }
}
export default connect(
    (store) => {return {login: store.login}},
    (dispatch) => bindActionCreators({
        getLogin
    }, dispatch)
)(RegistrationScreen);

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent:'center',
        padding:30
    }
});