import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native';

export default class StartScreen extends React.Component {
    static navigationOptions = {
        title: 'TestApp',
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTitleStyle: {
            color: '#FFFFFF'
        },
        headerLeft:null
    };
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.button} onPress={() => navigate('Login')}>
                    <Text style={styles.btn}>Login</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => navigate('Registration')}>
                    <Text style={styles.btn}>Registration</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent:'center',
        padding:30
    },
    button: {
        justifyContent:'center',
        alignItems: 'center',
        backgroundColor:'#036fe9',
        height: 40,
        marginTop:20,
    },
    btn: {
        color:'#fff'
    }
})