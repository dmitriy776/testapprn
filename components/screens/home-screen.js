import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native';
import firebaseApp from '../../firebase/firebase';
import {connect} from 'react-redux';

class HomeScreen extends React.Component {
    static navigationOptions = {
        title: 'Home',
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTintColor: '#FFFFFF',
        headerLeft:null
    };
    _signOut() {
        firebaseApp
        .auth()
        .signOut()
        .then(() => this.props.navigation.navigate('Start'))
        .catch(error => alert(error.message));
    }
    render() {
        return (
            <View style={styles.container}>
                <Text>User Login: {this.props.login}</Text>
                <TouchableOpacity style={styles.button} onPress={this._signOut.bind(this)}>
                    <Text style={styles.btnreg}>Log out</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
export default connect(
    (store) => {return {login: store.login}}
)(HomeScreen);

styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent:'center',
        padding:30
    },
    button: {
        justifyContent:'center',
        alignItems: 'center',
        backgroundColor:'#036fe9',
        height: 40,
        marginTop:20,
    },
    btnreg: {
        color:'#fff'
    }
})