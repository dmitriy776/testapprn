import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput
} from 'react-native';

export default class AuthForm extends React.Component {
    constructor() {
        super();
        this.state = {
            email: '',
            pass: ''
        }
    }

    _authForm() {
        this.props.addData(this.state.email, this.state.pass);
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    onChangeText={email => this.state.email = email}
                    style={styles.input}
                    placeholder="Your Email"
                    autoCorrect={false}
                    keyboardType="email-address"
                    autoCapitalize="none"
                />
                <TextInput
                    onChangeText={pass => this.state.pass = pass}
                    style={styles.input}
                    placeholder="Your Password"
                    secureTextEntry = {true}
                />
                <TouchableOpacity style={styles.button} onPress={this._authForm.bind(this)}>
                    <Text style={styles.btn}>{this.props.btnName}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent:'center',
        padding:30
    },
    input : {
        justifyContent:'center',
        height: 40,
        marginTop:20,
        padding:10
    },
    button: {
        justifyContent:'center',
        alignItems: 'center',
        backgroundColor:'#036fe9',
        height: 40,
        marginTop:20,
    },
    btn: {
        color:'#fff'
    }
});