import {createStore, combineReducers} from 'redux';
import loginReducer from '../reducers/get-login-reducer';

const reducer = combineReducers({
    login: loginReducer
});

const initialState = {
    login: ''
};

const store = createStore(reducer, initialState);


export default store;