import React from 'react';
import {
    StyleSheet,
    View,
} from 'react-native';
import firebaseApp from '../../firebase/firebase';
import AuthForm from '../auth-form/auth-form';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import getLogin from '../../actions/getLoginAction';

class LoginScreen extends React.Component {
    constructor() {
        super();
        this.state = {
            email: '',
            pass: ''
        }
    }

    _login() {
        firebaseApp.auth()
        .signInWithEmailAndPassword(this.state.email, this.state.pass)
        .then(() => {
            this.props.getLogin(this.state.email);
            this.props.navigation.navigate('Home');
        })
        .catch(function(error) {
            alert(error.message);
        });
    }

    _authentication(email, pass) {
        this.setState({
            email,
            pass
        }, this._login);
    }

    static navigationOptions = {
        title: 'Login',
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTintColor: '#FFFFFF',
    };

    render() {
        return (
            <View style={styles.container}>
                <AuthForm addData={this._authentication.bind(this)} btnName={'Login'}/>
            </View>
        )
    }
}
export default connect(
    (store) => {return {login: store.login}},
    (dispatch) => bindActionCreators({
        getLogin
    }, dispatch)
)(LoginScreen);

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent:'center',
        padding:30
    }
});