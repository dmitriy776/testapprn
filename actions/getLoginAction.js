export default function(login) {
    return {
        type: 'GET_LOGIN',
        login
    }
}