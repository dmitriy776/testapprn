export default function (state = '', action) {
    switch (action.type) {
        case 'GET_LOGIN':
            return `${action.login}`;
        default:
            return state;
    }
}