import React from 'react';
import { StackNavigator } from 'react-navigation';
import StartScreen from './components/screens/start-screen';
import LoginScreen from './components/screens/login-screen';
import RegistrationScreen from './components/screens/registration-screen';
import HomeScreen from './components/screens/home-screen';
import store from './state/state.js';
import {Provider} from 'react-redux';

const TestRnApp = StackNavigator({
    Start: { screen: StartScreen },
    Login: { screen: LoginScreen },
    Registration: { screen: RegistrationScreen },
    Home: { screen: HomeScreen },
});

export default class App extends React.Component {
  render() {
    return (
        <Provider store={store}>
            <TestRnApp />
        </Provider>
    );
  }
}


